# Summary

* [Willkommen](README.md)

## OER-Workflow
* [Workflow zur OER-Erstellung](Inhalt/Ablage/workflow.md) 
* [Wie kann man dieses Projekt forken?](Inhalt/Ablage/how-to-fork.md) 

## Good Practice aus Niedersachsen

## Die Lehrveranstaltung - Basiskompetenzen Digitalisierung

## OER-Sammlung
* [Podcast](Inhalt/Ablage/podcast.md) 

## Projektinformationen

* [Impressum](Inhalt/impressum.md)
* [Datenschutzerklärung](Inhalt/datenschutzerklärung.md)
