# Spuren im Netz
Eine Methodensammlung zu den Themen Privatsphäre und Metadaten
<center><img src="https://github.com/okfde/edulabs/blob/master/assets/img/projects_extern/Illustration_Spuren_im_Netz_CCC.jpg?raw=true"></center>
<p style="font-size:10pt;">
Illustration: Daria Rüttimann. Lizenz: <a href='https://creativecommons.org/licenses/by/4.0/'>CC-BY 4.0</a>
</p>

## Zusammenfassung

Im Workshop werden Teilnehmende anhand verschiedener Medien für das Thema Privatsphäre im Netz sensibilisiert. Die Beispiele rund um Instagram, WhatsApp, aber auch die Vorratsdatenspeicherung sind nah an der Lebenswelt von Jugendlichen. Anhand von Metadaten diskutieren die Lernenden privatwirtschaftliche und staatliche Überwachung. Außerdem werden die Geschäftmodelle von sozialen Netzwerken und damit einhergehende Gefahren thematisiert sowie Möglichkeiten sich zu schützen.

Dieser Workshop entstand im Rahmen von [Chaos Macht Schule](https://ccc.de/schule), einem Bildungsprojekt des Chaos Computer Clubs. Der Workshop wurde zahlreiche Male von Menschen in ihrer Freizeit erfolgreich durchgeführt. Die Dokumentation des Workshops ist gemeinsam mit der [edulabs](https://edulabs.de/)-Community erarbeitet worden und soll Interessierten die Möglichkeit geben, die Themen Privatsphäre und Metadaten zu vermitteln. 

Feedback ist ausdrücklich erwünscht: schule@berlin.ccc.de

## Eckdaten zum Material
* **Zeitbedarf:** Workshop ca. 90 Minuten (mehr wäre besser)
* **Zielgruppe:** Jugendliche (ca. ab der 5. Klasse), Erwachsene
* **Vorbereitungszeit:** ca. 3h
* **Ziel:** Neutral darstellen, welche Metadaten durch die Nutzung digitaler Kommunikationsmittel entstehen. Es soll keine politische Agenda kommuniziert werden.
    * Die Lernenden erklären Daten und Metadaten sowie ihre Bedeutung
    * Die Lernenden bewerten Chancen und Risiken von persönlichen Daten sowie ihre kommerziellen und staatlichen Verwendungsmöglichkeiten 
    * Die Lernenden bewerten, inwiefern sie sich vor privater, staatlicher und kommerzieller Überwachung schützen können. 
* **Methode:** Mediendiskussionen - im Folgenden werden verschiedene Fotos, Visualisierungen und Videos mit passenden Diskussionsfragen dargestellt
    * Die Fragen zeichnen ein Bild, welche Daten erfasst werden, was mit ihrer Hilfe über eine Person herausgefunden werden kann und welche individuellen Konsequenzen daraus resultieren: 
        * Wo wohnt er? 
        * Was isst er? 
        * Wann schläft er?
* **Mögliche Unterrichtskontexte:** 
    * Projekttag 
    * Ethik 
    * Englisch (Medienkommunikation, Überwachung etc.) 
    * Geschichte (totalitärer Staat)
    * (möglicherweise ein guter Aufhänger, um die Handy-Nutzung generell zu besprechen)
* **Folgende Themen werden behandelt:**
    1. Die Metadaten in Fotos
    2. Instagram und Privatsphäre
    3. WhatsApp und Metadaten
    4. Facebook: Analyse eines Freundschafts-Graphen
    5. Vorratsdatenspeicherung und Handy-Daten
    6. Diskussion und Zusammenführung

---

### (1) Die Metadaten in Fotos
**Ziele:**
* Die Lernenden erklären, was Metadaten allgemein sowie in Bildern sind.

**Einleitung:**
Es wird ein Bild gezeigt, aus dem scheinbar kaum etwas über den Fotografen herausgelesen werden kann. Der Workshopleiter diskutiert mit den Lernenden die angegebenen Fragen. Bei den ersten Fragen geht es vor allem darum, die Teilnehmenden auf die Metadaten in den Bildern hinzuleiten und Spannung aufzubauen. Deshalb sind sie eher rethorisch und zielen nicht auf konkrete Antworten ab. Wurde das Thema Metadaten von den Lernenden oder dem Workshopleiter dargestellt, wird der [Online-Exif-Viewer](http://exif.regex.info/exif.cgi) von der Lehrkraft geöffnet, um die Metadaten in dem Bild anzusehen. Zum einen soll die GPS-Position auf der digitalen Karte des Tools angezeit und hineingezoomt werden. Damit wird die Genauigkeit der Information kenntlich gemacht. Außerdem wird auf das Satelitenbild umgeschaltet, um den  Teilnehmenden zu zeigen, dass sich sogar die Aufnahmeposition innerhalb Haus auslesen lässt.

**Medium:**
![](https://github.com/okfde/edulabs/blob/master/assets/img/projects_extern/CMS_Methodensammlung_Metadaten.jpg?raw=true)
<center>Quelle: Pexels, CC0-Lizenz</center>

**Fragen & Antworten:**
* Was könnt ihr auf dem Bild sehen? Seht genau hin, es enthält überraschend viele Informationen. (spätere Aufforderung: Versucht mal ein wenig hinter die Kulissen zu blicken)
    * Teilnehmende beschreiben, was auf dem Bild zu sehen ist, wissen aber in der Regel nicht, worauf man hinaus möchte
* Würde es sich lohnen bei ihm/ihr einzubrechen?
    * TN: Schwer zu sagen, da keine Wertgegenstände zu sehen sind 
* Wo müssten wir hin, um einzubrechen?
    * die Teilnehmenden haben in der Regel keine Antwort
* Ist es in Ordnung, ein solches Bild online zu posten?
    * Hier gibt es Pro- und Contra Argumente. Es hängt vom persönlichen Geschmack ab, ob man sein Schlafzimmer posten mag.
* Wer kann erklären, was Metadaten sind?
    * Metadaten sind Daten über andere Daten. Am Beispiel einer Nachricht sind die Daten die Nachricht selbst, die Metadaten sind unter anderem, wer die Nachricht an wen gesendet hat, um wieviel Uhr sie versendet wurde etc. Welche Metadaten in einem Bild gespeichert sein können, wird nun ausgelesen.

**Weiterführende didaktische Ideen:**
* Teilnehmende können eigene Bilder aufnehmen und untersuchen, welche Metadaten enthalten sind. 
* Es kann experimentiert werden, wie sich die Einstellungen am Smartphone auf die Metadaten in aufgenommen Bildern auswirken.
* Über einen Exif-Editor wie beispielseweise [Thexifer](https://www.thexifer.net) lassen sich Metadaten in Bildern manipulieren
* Mit der App [phyhox](http://phyphox.org/de/) (iOs/Android) lassen sich die Messwerte zahlreicher Sensoren auf dem Smartphone anzeigen.

**Hintergrundinformationen:**
<details>
  <summary>
      <strong>Hier klicken</strong>
  </summary>
  <p>
Das Teilen von selbstaufgenommenen Fotos ist bei Kindern und Jugendlichen sehr beliebt. Schließlich möchte man auch die Freunde an den eigenen Erlebnissen teilhaben lassen.

Metadaten sind Daten über andere Daten. Bei einer Nachricht sind beispielsweise die Nachricht selbst die Daten, die Metadaten sind Informationen, wie Absender und Empfänger, der Zeitpunkt des Absendens, die Länge der Nachricht, etc. Fotos können neben offensichtlichen Metadaten, wie dem Dateinamen, auch zahlreiche weitere enthalten. Dazu gehören beispielsweise eine GPS-Position, die Richtung der Kamera bei der Aufnahme, die Höhe über dem Meeresspiegel, die Geschwindigkeit etc.

Für viele Anwendungen sind diese sehr hilfreich. Wer beispielsweise nach Jahren wieder die Urlaubsfotos ansieht, kann leicht herausfinden wo ein bestimmtes Bild aufgenommen wurde. Auf Instagram lassen sich beispielsweise alle Fotos anzeigen, die an einem bestimmten Ort aufgenommen wurden ([Fotos am  Brandenburger Tor auf Instagram](https://www.instagram.com/explore/locations/213310140/brandenburger-tor/)).

Neben einigen positiven Nutzungsmöglichkeiten können Metadaten aber auch problematisch werden, wenn Menschen sich nicht darüber bewusst sind, welche Daten sie hinterlassen und wie andere diese weiterverwenden. Aus diesem Grund entfernen beispielsweise viele Foto-Upload-Plattformen diese Metadaten (aber nicht alle). Einerseits schützt dies die Privatsphäre der Nutzenden, andererseits stellt sich die Frage, ob man damit einverstanden ist, dass eine Plattform die eigenen Medien ungefragt verändert. Auch wenn kommerzielle Social Media Plattformen diese Informationen nicht mehr anzeigen, ist davon auszugehen, dass sie diese intern trotzdem speichern und weiterverarbeiten. Schließlich sind Daten ihr Geschäftsmodell und sie speichern sonst auch alles, was sie an Informationen über Nutzende erhalten können. Überprüfen lässt sich das nur schwer, da von außen neimand nachvollziehen kann, was im Inneren dieser Systeme passiert.
  </p>
</details>

---

### (2) Instagram und Privatsphäre

**Ziele:**
* Die Lernenden bewerten die Nutzung von Metadaten im positiven sowie negativen Sinne.

**Einleitung:**
Nachdem wir gesehen haben, dass Bilder Metadaten (z.B. Ortsinformationen) enthalten, wollen wir sehen, wie man diese kreativ nutzen kann. 

**Medium:**
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Mz3E8S3QP0w?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<center><a href="http://www.metaversa.de/web/doks/Netzspionage_cc.mov">Eine frei lizenzierte Version des Videos steht hier zum Download</a></center>

**Fragen & Antworten:**
* Wie gefällt euch das Video?
    * TN: Gut, wir wären auch gerne mal Jonathan und würden die überraschten Leute ansprechen
    * TN: Nicht gut, wir möchten ja nicht mit so vielen Details über uns in der Öffentlichkeit angesprochen werden
* Wie kann man sich schützen?
    * Instagram-Profil auf "privat" stellen
    * Ortung auf dem Smartphone deaktivieren
    * Ortung beim Upload von Instagram deaktivieren

**Weiterführende didaktische Ideen:**
* Wenn die Lernenden jünger sind, sollen sie den Film kurz zusammenfassen, um sicherzugehen, dass sie ihn verstanden haben.
* Aus dem Video geht nicht nur hervor, dass Metadaten vorhanden sind, sondern auch, dass sehr persönliche Details über die Ausgespähten ermittelbar sind ("Mit guter Ernährung hast du es aber nicht so?"). 

**Hintergrundinformationen:**
<details>
  <summary>
    <strong>Hier klicken</strong>
  </summary>
  <p>
Instagram ist soziales Netzwerk, auf dem Fotos mit der Öffentlichkeit und Followern geteilt werden. Es ist unter Kinder und Jugendlichen sehr beliebt.

Wie wir in dem ersten Beispiel gesehen haben, können Fotos Metadaten enthalten. Diese kann man samt dem Foto bei Instagram hochladen, deshalb wird dort bei einigen Bildern auch angezeigt, wo sie aufgenommen wurden (siehe Link oben). 

Auch wenn das Video mindestens zu Teilen gestellt ist, demonstriert es aber anschaulich, was sich mit GPS-Daten alles anstellen lässt  und warum das ein Problem für die Privatsphähre werden kann. Plattformen wie [beispielsweise Instagram](https://www.instagram.com/developer/) bieten Programmierern die Möglichkeit, bestimmte Daten über Programmierschnittstellen, sogenannte Application Programming Interfaces (API), abzurufen, um diese in einem anderen Kontext außerhalb der Plattform weiterzuverwenden. Beispiele für solche Apps von Dritten sind die im Video erwähnten. Die Daten konnten von Dritten über eine Programmschnittstelle (API) abgerufen werden. 

Weil APIs aber von Zeit zu Zeit aktualisiert werden, kann man die im Video vorgestellten Daten heute nicht mehr so leicht abrufen. Deshalb funktioniert die im Video vorgestellte App auch nicht mehr. Trotzdem sollte man sich darüber bewusst sein, dass diese Ortungsdaten bei Instagram gespeichert sind und sie von Instagram mit Sicherheit weiterverwendet werden. Wie diese Daten weiterverarbeitet werden, verrät Instagram leider nicht.
  </p>
</details>

---

### (3) WhatsApp und Metadaten
**Ziele:**
* Die Lernenden erklären, dass Online-Zeiten viel über den eigenen Tagesablauf aussagen.
    * Sie erklären auch, dass hinter diesen Daten Geschäftsinteressen stehen.
* Die Lernenden bewerten, dass diese Möglichkeit im positiven sowie negativen Sinne genutzt werden kann

**Einleitung:**
Was kann ich mit WhatsApp über einen anderen User in Erfahrung bringen, ohne dass er die heimliche Beobachtung bemerkt? Im folgenden Experiment "Online Status Monitor" wurde für viele Nutzende automatisiert abgerufen, ob die jeweilige Person gerade online ist. Die Online-Zeiten wurden über einen Zeitraum kontinuierlich abgerufen und gespeichert. Die angefallenen Daten werden jeweils auf Profilseiten für einzele Benutzer aufbereitet. Die Profile zeigen jeweils eine anonymisierte Telefonnummer, das anonymisierte Bild, die Statusnachricht sowie das durchschnittliche Nutzungsverhalten. Weil es sich um reale Personen handelt, wurden die Daten anonymisiert.

**Medium:**
Es werden WhatsApp Profile analysiert, die über <a href="https://www.onlinestatusmonitor.com/ ,j">onlinestatusmonitor.com</a> öffentlich eingesehen werden können. Es wird in erster Linie der Abschnitt "Average Usage Time/Connections by Hour" auf den Seiten betrachtet. Bei folgenden Beispielprofilen nehmen wir an, dass es sich um junge Nutzende handelt.

1. https://onlinestatusmonitor.com/user_statistics/#bef10fc58a3da752706d03100cf0c889
Schlafenszeit zwischen 22 und 6 Uhr; Start der Aktivität zwischen 6 und 9 Uhr. Überdurchschnittliche Kommunikation um 9, 12-13, 16-18 und 20-22 Uhr. Starker Anstieg morgens und am Abend (Eltern könnten die Bett-Zeit bestimmen). Eine insgesamt geringe Kommunikationszeit deuten auf jüngeren Nutzenden hin.
2.  https://onlinestatusmonitor.com/user_statistics/#8eb75d38de9595e17b8a454e6f13c380
Schlafenszeit zwischen 23 und 7 Uhr, mit einigen Aktivitäten zwischen 0 und 2 Uhr (es gibt Nächte die länger gehen). Deutlich schärfere Peaks um 9, 13 und 19-21 Uhr in der Komminikation. Dem Anschein nach freie Zeit wird deutlich intensiver für Kommunikation genutzt, während in den Zwischenzeiten (10-12 und 15-18 Uhr) die Kommunikation stärker Einbricht. Hier scheinen andere Aktivitäten (Arbeit, Schule, Studium) den Nutzer/die Nutzerin stärker zu beanspruchen, so dass weniger Zeit zur Kommunikation zur Verfügung steht.
3. https://onlinestatusmonitor.com/user_statistics/#9f4166a40d0a17ff57d918f4ef46707c
Schlafenszeit zwischen 0 und 6 Uhr, Flacher anstieg und Abfall der Aktivität zeigt eine relativ hohe Flexibilität in den Zeiten. Innerhalb der aktiven Zeit gibt es praktisch keine ausgeprägten Peaks, das deutet auf einen wenig Strukturierten Arbeits oder Schultag hin.

Natürlich sind die persönlichen Daten der jeweiligen Profile nicht bekannt, aber (1.) würde einen typischen Verlauf eines Lernenden (12-15) entsprechen. as 2. Profil könnte man bei vielen Jugenlichen (16-18) und das 3. wäre dann ein Profil für junge Studierende.

**Fragen & Antworten:**
* Wie funktioniert WhatsApp?
    * Teilnehmende beschreiben die Funktionalität kurz.
* Was muss man machen, um eine andere Person bei WhatsApp anschreiben zu können?
    * Man muss lediglich die Nummer der Person im eigenen Adressbuch des Smartphones speichern, dann kann man sie auf WhatsApp anschreiben.
* Angenommen, ich habe die Nummer in meinem Smartphone gespeichert, die Person aber meine nicht. Was kann ich alles von dieser Person sehen, ohne dass sie etwas davon merkt?
    * Foto
    * Online-Status
    * Statusnachricht
* Betrachtung des Kommunikationsverhaltens einzelner Personen anhand der Medien oben. Was könnt ihr sehen? (dies wird für mehrere Profile wiederholt)
    * Der Tagesablauf einer Person ist durch die aufgezeichneten Online-Zeiten sehr gut ablesbar. Man kann sehen, wann sie aufsteht und ins Bett geht. Oft kann man die Mittagspause erkennen. Man kann sehen, wann die Person am meisten Zeit hat, mit anderen zu kommunizieren. Daraus lassen sich viele Rückschlüsse über die berufliche Situation ziehen.
* Könnte das ein Problem sein? Wen könnten die Daten interessieren?
    * Eltern, die mitbekommen, dass ihre Kinder nicht schlafen.
    * Lehrende, die wissen, warum Kinder im Unterricht so unkonzentriert sind. 
    * Krankenkassen, die wissen, dass jemand, der generell wenig schläft, vermutlich irgendwann krank wird, deshalb ein erhöhtes Risiko ist und die Krankenkasse mehr kostet
    * mögliche Arbeitgeber / Bewerbungsgespräch. Denn wer wenig schläft, kann sich nicht konzentrieren. Wer den ganzen Tag privat kommunziert, würde dies möglicherweise auch während der Arbeitszeit machen.
* Für wen wären diese Daten vielleicht auch Geld wert?
    * Beisspielsweise wären die Daten für Krankenkassen oder Arbeitgeber interessant, da sie mit den Menschen Geld verdienen wollen.
    * Für soziale Netzwerke sind diese Daten interessant, damit sie ihren Nutzern personalisierte Werbung zeigen können.
    * Die Lehrkraft lenkt die Diskussion dahin, dass bei sozialen Netzwerken die Nutzer das Produkt sind und die Werbetreibenden die Kunden. Werbetreibende zahlen dafür, dass Nutzern Werbung angezeigt wird. Aber auch andere sind bereit, Geld für die persönlichen Daten zu zahlen, die sie dann evtl auch gegen die entsprechenden Nutzer verwenden möchten (siehe Arbeitgeber, Krankenkasse).
* Möchtet ihr jedem, der eure Handynummer kennt, auch in der Form euren Tagesablauf offenlegen?
    * Natürlich möchte man es nicht. Doch über WhatsApp lassen sich diese Daten auch von Dritten auslesen und weiterverarbeiten.
* Ist es okay, dass Freunde die eigene Telefonnummer ungefragt weitergeben?
    * Nein, aber das macht WhatsApp automatisch, sobald man es installiert. Lernende geben diese Daten also weiter, sobald sie WhatsApp verwenden.

**Weiterführende didaktische Ideen:**
* Lernende können ihren eigenen Tagesablauf anhand der WhatsApp-Nutzung skizzieren.
* Mit Lernenden über Alternativen zu WhatsApp diskutieren
    * Es gibt zahlreiche Alternativen, doch oft werden diese nur von sehr wenigen Kontakten verwendet. Online-Zeiten fallen fast überall an, jedoch ist das Datensammeln nicht bei jeder App das Geschäftsmodell.
* Diskussion: Angenommen, WhatsApp würde die Privatsphäre der Nutzenden besser schützen und die Informationen nicht jedem zugänglich machen. Wäre das Problem dann behoben?
    * Es wäre sicher besser, aber das grundlegende Problem mit dem Geschäftsmodell besteht auch weiterhin. WhatsApp kann die Daten schließlich nach Belieben weiterverwenden oder gar Weiterverkaufen. Auch wenn WhatsApp letzters bisher nicht macht, hat man keine Garantie, wie es in der Zukunft sein wird. 
    
**Hintergrundinformationen:**
<details>
  <summary>
    <strong>Hier klicken</strong>
  </summary>
  <p>
      WhatsApp ist mit rund <a href="https://de.statista.com/statistik/daten/studie/285230/umfrage/aktive-nutzer-von-whatsapp-weltweit/">1,5 Milliarden Nutzende der populärste Messenger</a>, unter Jugendlichen ist das soziale Netzwerk sehr beliebt. Wann immer sie können, nutzen sie es intensiv, um mit ihren Kontakten im Austausch zu bleiben. Abseits von Menschen, die besonderen Wert auf ihre Privatsphäre legen, verwendet den Messenger nahezu jeder auf seinem Smartphone. Nutzer identifizieren sich auf dem sozialen Netzwerk mit der eigenen Mobilfunknummer, über die man von andere NutzerInnen gefunden wird. Nach der Installation der App auf dem Smartphone lädt WhatsApp zunächst das gesamte eigene Adressbuch auf die eigenen Server und überprüft, welche der Kontakte auch WhatsApp nutzen. Danach werden alle Kontakte mit WhatsApp aufgelistet und man kann sie direkt kontaktieren, ohne weiter Informationen austauschen zu müssen. Das ist für den Nutzer besonders einfach und sicher einer der Gründe für die große Verbreitung des Messengers. Auf der anderen Seite wird der automatische Upload der eigenen Adressbuchs von Datenschützern heftig kritisiert, Juristen zweifeln, ob dies in Deutschland <a href="http://www.spiegel.de/netzwelt/netzpolitik/whatsapp-upload-von-kontaktdaten-ist-illegal-a-1154667.html">rechtlich zulässig</a> ist. Der Vollständigkeitshalber sei erwähnt, dass es theoretisch möglicht ist, der Messenger ohne Zugriff auf das Adressbuch zu betreiben. Diese ermöglichen die Einstellungen der Zugriffsrechte, die mittlerweile jedes moderne Smartphone Betriebssystem zur Verfügung stellen. Praktisch wäre der Messenger dann aber nur noch schwer zu benutzen. 

Sobald man die Telefonnummer von eines anderen WhatsApp-Nutzers kennt, lassen sich sowohl das Profilfoto, eine Statusnachricht und der Online-Status anzeigen. Die Webseite [https://www.onlinestatusmonitor.com/](https://www.onlinestatusmonitor.com/) hat von vielen zufällig generierten Telefonnummern den Online-Status kontinuierlich abgerufen und gespeichert. Details zum Ablauf des Projekt finden sich auf der Webseite. Auf der Webseiten werden verschiedene Profile angezeigt, diese beinhalten auch einen Graphen, wann eine bestimmte Person online oder nicht online war. Aus diesem Graphen lässt sich problemlos der Tagesablauf der Person rekonstruieren, obwohl man nur die Online-Zeiten kennt. Wann ist die Person morgens aufgestanden, wann abends zu Bett gegangen? Auch die MIttagspause lässt sich oft gut erkennen oder zu welcher Zeit eine Person wohl arbeitet oder zur Schule geht. Die Nutzung spiegelt den Tagesablauf so deutlich wider, weil der Messenger von vielen sehr intensiv verwendet wird. Jugendliche rufen beispielsweise morgen nach dem Aufstehen ihre neuen Nachrichten ab und schreiben vorm Schlafengehen die letzten.

Der Online-Status ist praktisch, weil man so sehen kann, ob ein Kommunikationspartner verfügbar ist und wohl direkt auf eine Nachricht antworten würde. Bei WhatsApp lässt sich in den Privatsphäreeinstellungen zwar festlegen, dass niemand den eigenen Online-Status sehen darf. Dieser wird dann nicht mehr innerhalb der App für andere Nutzer angezeigt. Doch während Nutzer sich mit den Einstellungen auf der sicheren Seite sehen, ist es über Tools von Drittanbietern auch [möglich](https://maikel.pro/blog/en-whatsapp-privacy-options-are-illusions/) den Online-Status auszulesen, auch wenn dies vom Nutzer deaktiviert wurde.

Wer die Kontakte der Person sind, lässt sich von außen genausowenig wie die Inhalte der Nachrichten einsehen. Nicht einmal WhatsApp kann Nachrichten noch mitlesen, seitdem der Dienst im Jahre 2017 die Ende-zu-Ende-Verschlüsselung eingeführt hat. Aber derart persönliche Informationen wie das Nutzungsverhalten lassen sich problemlos von Dritten abrufen - ohne dass WhatsApp etwas dagegen übernimmt. WhatsApp selber, als Betreiber der Anwendung, hat natürlich Zugriff auf viel mehr Informationen über seine Nutzer wie beispielsweise die jeweiligen Adressbücher.

Generell ist das Geschäftsmodell der großen sozialen Netzwerke das Sammeln von persönlichen Daten ihrer Nutzer. Je mehr Informationen die sozialen Netzwerke über ihre Nutzer wissen, desto besser lässt sich ihnen personalisierte Werbung anzeigen und desto mehr Umsatz kann das soziale Netzwerk mit dem Nutzer machen. Dafür werden zusätlich sogar Daten von anderen Firmen [angekauft](https://netzpolitik.org/2018/kurz-erklaert-wie-facebook-dich-ausspioniert-auch-ohne-dein-mikro-abzuhoeren/), es werden also Daten aus verschiedenen Quellen kombiniert, denn da. Da WhatsApp und Instagram alle zu Facebook gehören, ist es für den Betreiber natürlich auch von Interesse, diese Daten plattformübergreifend zu kombinieren. Die sozialen Netzwerke verkaufen die Daten ihrer User in der Regel nicht, wie oft fälschlicherweise behauptet wird. Doch wie der Skandal um Cambridge Analytica gezeigt hat, verlassen große Datensätze auch [auf anderem Wegen](https://netzpolitik.org/2018/cambridge-analytica-was-wir-ueber-das-groesste-datenleck-in-der-geschichte-von-facebook-wissen/) die Plattform, ohne dass Nutzer immer die Kontrolle darüber haben.

Obwohl WhatsApp-Gründer Jan Koum in Interviews betonte, wie wichtig ihm die Privatsphäre seiner Nutzer sei und dass er den Dienst niemals verkaufen würde, verkaufte er ihn im Jahr 2015 für rund 19 Milliarden Dollar an Facebook. Bei den damals 450 Millionen Nutzern bedeutet es, dass Facebook rund 42 Dollar pro Benutzer bezahlt hat. Auch wenn Koum und Facebook-Gründer Marc Zuckerberg nach dem Deal versicherten, dass die WhatsApp-Daten nicht mit Facebook verknüpft werden, wurde genau das im Jahre 2017 umgesetzt - was zumindest in Deutschland noch [rechtlich umstritten](http://www.sueddeutsche.de/digital/datenschutz-was-facebook-mit-ihren-whatsapp-daten-vorhat-1.3511586) ist. 

Das Beispiel zeigt deutlich, dass man über persönliche Daten, die man hinterlassen hat, die Kontrolle verloren hat. Auch die Daten von Facebook-Nichtnutzern landeten mit dem Deal bei Facebook. Von wem Facebook vielleicht eines Tages aufgekauft wird, bleibt abzuwarten. Auch zeigt das Beispiel, dass man sich nicht auf die Aussagen von Plattformbereibern verlassen darf. Es geht hier eben um Firmen und ihre Geschäftsinteressen. Man darf bei den regelmäßig in den Medien präsenten Gründern, die oft symphatisch wirken und nicht wie klassische Geschäftsleute auftreten, nicht vergessen, dass sie nicht die eigenen Freunde sind, für die man sie manchmal halten könnte.

Tatsächlich gibt es zahlreiche Alternativen zu WhatsApp, deren Geschäftsmodell nicht auf dem Sammel von persönlichen Daten beruht. Auch wenn Alternativen natürlich auch [Vor- und Nachteile](https://ssd.eff.org/) mit sich bringen, sind viele aus Privatsphäresicht gegenüber WhatsApp zu bevorzugen. Ein Problem ist aber, dass die Verbreitung von Alternativen verglichen mit WhatsApp gering ist. Trotz aller Kritik an WhatsApp war der Messenger einer der ersten für das Smartphone, die Privatsphäre-freundlichen folgten erst deutlich später. WhatsApp ist kostenlos und auf für Laien einfach zu bedienen. Aus diesen Gründen ist WhatsApp der klare Marktführer, auch wenn manche Alternativen in vielen Aspekten deutlich besser sind. Eine Möglichkeit, die Risiken etwas zu minimieren, ohne auf den Kontakt zu Freunden zu verzichten, wäre es neben WhatsApp noch einen Privatsphäre-freundlicheren Messenger parallel zu nutzen.

  </p>
</details>


---

### (4) Facebook: Analyse eines Freundschafts-Graphen
**Ziele:**
* Die Lernenden erklären, was aus Freundschaftsgraphen gelesen werden kann.
* Die Lernenden bewerten Freundtschaftsgraphen.

**Einleitung:**
Das folgende Bild zeigt den Freundeskreis einer untersuchten Person. Es wird ein Graph eines Facebook-Freundschafts-Netzwerks gezeigt, jeder Punkt in dem Bild ist eine Person, zwischen ihnen liegt jeweils eine Kante, wenn sie auf Facebook miteinder befreundet sind. Wir haben die Person in der Mitte untersucht, alle anderen Personen in dem Graphen sind mit ihr befreundet. Außerdem zeigt der Graph, wie die Freunde untereinander befreundet sind. Schon auf den ersten Blick erkennt man die unterschiedlichen Freundeskreise. Die Freundschaftsbeziehungen bieten Facebook also wertvolle Informationen über ihre User. Die Lernenden analysieren das Netzwerk anhand der aufgeführten Fragen.

**Medium:**
![](https://raw.githubusercontent.com/okfde/edulabs/master/assets/extern/chaos_macht_schule_oer/facepalm.png)
<center>Lizenz: CC-BY Yorn / Chaos macht Schule</center>

**Fragen & Antworten:**
* Was könnten das für Freundeskreise sein? (einige können die Teilnehmenden selbst herausfinden, bei manchen muss der Workshopleiter oder die -leiterin die Lösung irgendwann in den Raum werfen)
    * Unten: Schule (viele Personen, alle untereinander vernetzt, Geschlechterverhältnis etwa ausgeglichen)
    * Rechts unten: Sportverein (etwa Mannschaftsstärke, fast nur Männer)
    * Rechts oben: Universität (die Person studiert mittlerweile, deshalb gibt es Verbindungen zur Schule). Anhand des Geschlechterverhältnis lässt sich diskutieren, was es für ein Studiengang sein könnte
    * Oben: Familie (Geschlechterverhältnis etwa ausgeglichen, nicht zu stark vernetzt, da die Elterngeneration Facebook nicht so intensiv nutzt)
    * Links: Burschenschaft. Der Vater der Person ist sowohl mit der Familie als auch mit der Burschenschaft verbunden. Der Sohn ist vor allem über den Vater Teil der Burschenschaft, obwohl er in dieser nicht sonderlich aktiv zu sein scheint, schließlich gibt es keine Verbindungen zu seinen anderen Kommilitonen.

**Weiterführende didaktische Ideen:**
* Dafür brauchen die Lernenden recht viel Zeit (ca. 10 min). Für Jüngere ist dieses Schaubild recht abstrakt.
* Beispiel für einen ähnliche Graphen für [E-Mail-Kontakte](https://immersion.media.mit.edu/demo)
 
**Hintergrundinformationen:**
<details>
  <summary>
    <strong>Hier klicken</strong>
  </summary>
  <p>
Unsere persönlichen Kontakte lassen sich als Graphen darstellen. Graphen sind Konzepte aus der Informatik, die aus Knoten und Kanten bestehen. Der im Workshop verwendete Graph beschreibt die Freundschaftsbeziehungen eines Facebook-Nutzers. 

Im gegebenen Graphen wird der Freundeskreis einer Person analysiert. Jeder Knoten (Kreis) im Graph stellt eine Person dar. Die untersuchte Person wird durch den Knoten in der Mitte repräsentiert. Alle anderen Personen (Knoten) in dem Graphen sind mit dieser Person auf Facebook freundschaftlich verbunden, dies wird durch die Kanten, die die Knoten verbinden, dargestellt. Außerdem wurde in einem zweiten Schritt untersucht, wie die Freunde untereinander vernetzt sind. Dies wird durch die Kanten und die räumliche Nähe zueinander abgebildet. Bei den blauen Personen handelt es sich um männliche, bei den pinken um weibliche und von den grauen ist kein Geschlecht bekannt. 

Die einzelnen Freundeskreise lassen sich klassifizieren. Der größte Freundeskreis unten bildet die Kontakte aus der ehemaligen Schule der untersuchten Person ab. Das erkennt man an der hohen Anzahl der Freunde, dem ausgeglichenen Geschlechterverhältnis sowie daran, dass alle untereinander auch verbunden sind. Beim Freundeskreis rechts unten handelt es sich um seinen Sportverein, da alle Beteiligten männlich sind, lassen sich Vermutungen anstellen, welche Art von Sport es sein könnte. Die untersuchte Person hat die Schule mittlerweile verlassen und studiert. Dies zeigt der Freundeskreis rechts oben. Weil ein paar MitschülerInnen mit zur Uni gewechselt sind, gibt es wenige Verbindungen zu der Schule. Auch hier lassen sich aufgrund des Geschlechterverhältnisses Vermutungen darüber anstellen, um welchen Studiengang es sich hierbei handeln könnte. Was noch fehlt, ist die Familie, sie ist der Freundeskreis oben. Hier ist das Geschlechterverhältnis einigermaßen ausgeglichen. Weil in der Familie Facebook nicht intensiv genutzt wird, ist der Grad der Vernetzung untereinander eher gering. Interessanterweise gibt es einen blauen Knoten, der sowohl zur Familie als auch zum verbleibenden Freundeskreis links gehört. Die besagte Person ist der Vater, der auch Konkakt zu ein paar SchulfreundInnen hat. Außerdem ist der Vater zusammen mit seinem Sohn gemeinsam Mitglied in einer Burschenschaft, die der verbleibende Freundeskreis auf der linken Seite darstellt.

Natürlich gibt der Graph nicht all die beschriebenen Informationen wider. Teilweise sind sie uns nur bekannt, weil wir die Gelegenheit hatten, uns das untersucht Profil sowie die Profile der Freunde anzusehen. Lässt man SchülerInnen aber raten, welcher Teilgraph welcher Freundeskreis ist, werden die Schule, der Sportverein oder die Familie direkt identifiziert.

Der eigene Freundeskreis sagt natürlich viel über einen selbst aus. Haben Freunde Metadaten angegeben, können sie mit einem solchen Graphen auch genutzt werden, um die eigene Person zu klassifizieren. Wer viele Freunde mit Schulden hat, hat möglicherweise auch selber Geldprobleme und erhält Kredite nur zu schlechten Konditionen. Die Rückschlüsse auf die untersuchte Person sind natürlich nur Vermutungen und müssen nicht zwingend richtig sein. Doch aufgrund solcher Informationen und daraus resultierenden Wahrscheinlichkeiten können Personen von anderen Menschen oder Algorithmen beurteilt werden. Das mag zu einem Problem werden, wenn man aufgrund einer politischen Einstellung der Freunde einen Job nicht bekommt oder von der Versicherung abgelehnt wird, weil es bereits mit Bekannten Probleme gab.

Natürlich ist Facebook nur ein stellvertretendes Beispiel. Durch Graphen lassen sich auf allen Plattformen die Kontaktbeziehungen darstellen. Beispielsweise könnten Graphen der WhatsApp- oder Instagram-Kontakte abgebildet werden. Das gilt auch für unsere Freundes- oder Kollegenkreise im analogen Leben - auch wenn wir sie möglicherweise gar nicht digital abgebildet haben.
  </p>
</details>

---

### (5) Vorratsdatenspeicherung und Handy-Daten
**Ziele:**
* Die Lernenden erklären, dass Metadaten nicht nur interessant für kommerzielle Anwendungen sind, sondern auch für staatliche Überwachung
* Die Lernenden bewerte die staatlichen Einsatzmöglichkeiten und ihre Wirksamkeit (Behauptung: Vorratsdatenspeicherung schützt vor Terroranschlägen)

**Einleitung:**
Nachdem wir nun verschiedene soziale Netzwerke betrachtet und teilweise diskutiert haben, wie man sich davor schützen kann, folgt zum Abschluss noch ein politisches Beispiel: die Erklärung der Vorratsdatenspeicherung und der Hintergrund der Visualisierung der Vorratsdaten von Malte Spitz.

**Medium:**
[Verräterisches Handy (Zeit Online)
](http://www.zeit.de/datenschutz/malte-spitz-vorratsdaten) 

**Fragen & Antworten:**
* Müssen am Telefon GPS oder Internet aktiviert sein, damit diese Daten über euch erhoben werden können?
    * Für die Bewegungsinformationen ist lediglich eine Telefonverbindung notwendig, da die Ortung über Mobilfunkmasten erfolgt - kein Internet, kein GPS ist nötig.
* Glaubt ihr, dass die Vorratsdatenspeicherung gegen Terrorismus hilft?
    * Hier sind viele Antworten möglich, es eignet sich eine Diskussion. Fakt ist: wer nicht verfolgt werden möchte (beispielsweise Terroristen), lässt sein Handy einfach zu Hause. Außerdem lassen sich Straftaten allenfalls im Nachhinein aufklären, im Regelfall aber nicht verhindern.
* Ist es verhältnismäßig, die ganze Bevölkerung mit der VDS anlasslos zu überwachen? 
    * Diskussion mit den Lernenden - Argumentationen: bisher haben hohe Gerichte die verschiedenen Versionen der VDS für unzulässig erklärt. Auf der anderen Seite betonen Sicherheitspolitiker immer wieder, wie wichtig das Instrument für die Strafverfolgung sei.

 
**Weiterführende didaktische Ideen:**
* Sollten SuS aus politischer Sicht kein Problem damit haben, eignet sich immer die Frage, ob sie einverstanden wären, wenn ihre Eltern in Echtzeit Zugriff auf diese Daten hätten.
* Mit den Lernenden können die Mobilfunkantennen besprochen werden, mit denen ihre Smartphones kommunizieren. Sie sind im Stadtbild sehr präsent.
* Auch könnten Triangulation und Handyortung über Mobilfunkmasten thematisiert werden.
* Android-Smartphones speichern in den Google-Accounts auch ein persönliches Bewegungsprofil, welches sich einsehen lässt

**Hintergrundinformationen:**
<details>
  <summary>
    <strong>Hier klicken</strong>
  </summary>
  <p>
Die Vorratsdatenspeicherung (VDS) bezeichnet die verdachtsunabhängige Speicherung von personenbezogenen Telekommunikations- und Standortdaten der gesamten Bevölkerung durch Netzbetreiber über einen definierten Zeitraum. Ermittler sollen diese zur Verbrechensbekämpfung verwenden können. Dies soll der Bekämpfung von Terrorismus dienen. Vor allem konservative Parteien sehen das Überwachungsinstrument als unersätzlich an, wogegen DatenschützerInnen auf die Unschuldsvermutung pochen und die VDS für einen unangemessenen Eingriff in die Privatsphäre halten. Aus den gespeicherten Metadaten können persönliche Profile erstellt werden. Geraten diese Daten in falsche Hände, könnte es weitreichende Folgen für die Betroffenen haben. Auf der anderen Seite wurde der Nutzen der VDS für die Verbrechensbekämpfung bis heute nicht nachgewiesen.

Die Geschichte der VDS reicht bis ins Jahr 2006 zurück und es gab mehrere Iterationen über sie, die stets von den Gerichten für unzulässig erklärt wurden. Nach europäischen Vorgaben wurde die erste VDS in Deutschland 2007 verabschiedet, um gegen die neuen Herausforderungen des internationalen Terrorismus gerüstet zu sein. Die VDS sah eine anlasslose 6-monatige Speicherung vor, wer mit wem, wann und wo kommuniziert hat. Gespeichert wurden nur Metadaten, keine Kommunikationsinhalte. Die Speicherung umfasste Telefonate, SMS, E-Mails sowie die Liste der aufgerufene Internetseiten. Im März 2010 erklärte das Bundesverfassungsgericht die VDS für unvereinbar mit dem deutschen Grundgesetz. Als Gründe wurden die nicht dezentrale und unsichere Speicherung genannt. 

Während die VDS in Kraft war und die Daten über alle BundesbürgerInnen gespeichert wurden, gab es eine öffentliche Debatte über die Verhältnismäßigkeit. Der Grünen-Politiker Malte Spitz hat sich 2010 die über ihn gespeicherten Vorratsdaten von der Telekom erkämpft und diese zusammen mit Open Data City und Zeit Online visualisiert. Die Visualisierung bereitet das sehr abstrakte Thema, was diese Daten aussagen, für die breite Bevölkerung anschaulich auf. Ausführliche Informationen finden sich im zugehörigen [Artikel auf Zeit Online](http://www.zeit.de/digital/datenschutz/2011-02/vorratsdaten-malte-spitz). 

Kurz darauf folgte das oben genanntes Gerichtsurteil. Doch war die VDS damit nicht Geschichte. Politiker besserten nach und führten eine neue, aus ihrer Sicht gesetzeskonforme Version der VDS ein, die im Oktober 2015 verabschiedet wurde. Mit dem neuen Gesetzentwurf sollten Telefonanbieter und Internetprovider verpflichtet werden, sogenannte Verkehrsdaten zehn Wochen lang zu speichern. Standortdaten sollen vier Wochen lang gespeichert werden. Außerdem wurde der neue Straftatbestand der Datenhehlerei eingeführt, falls die Daten in ungefugte Hände geraten sollten. Doch bereits im Jahre 2016 kippte der europäische Gerichtshof die VDS, weil eine anlasslose Speicherung nicht mit europäischem Recht vereinbar sei. Es ist fragwürdig, wie das deutsche Gesetz der VDS mit der europäischen Richtline vereinbar sein soll. Deshalb sind wir in Deutschland momentan in der Konfliksituation, dass zwar eine VDS gesetzlich vorgeschrieben ist, die Provider sie aber momentan mit Blick auf die europäische Richtlinie nicht umsetzen. Die Geschichte der VDS ist damit vermutlich noch nicht beendet.

SicherheitspolitikerInnen betonen bei der VDS zwar immer, dass der Eingriff in die Privatspähre gering sei, da die Daten nur kurz gespeichert werden und geregelt sei, dass ErmittlerInnen nur bei schweren Straftaten darauf Zugriff erhalten. Doch DatenschützerInnen befürchten, dass die VDS früher oder weiter ausgedehnt wird, sobald sie erst einmal etabliert ist. Nach der Einführung der VDS gab es direkt Forderungen, diese auch für die Verfolgung von sogenannten Raubkopieren einzusetzen oder Speicherfristen zu verlängern.

Technisch betrachtet benötigt die VDS übrigens keine Internetverbindung und auch kein GPS-Signal. Die VDS basiert lediglich auf den Funksignalen, die kontinuierlich zwischen dem Mobiltelefon und den Sendemasten ausgetauscht werden. Durch unterschiedliche Stärke der Funkverbindungen zu den drei umliegenden Funkmasten kann die Position des Telefons bis auf wenige Meter genau bestimmt werden. Diese Funkverbindung zwischen Smartphone und Funkmasten ist technisch notwendig, damit der Mobilfunkanbieter weiß, wo sich das Telefon gerade befindet. Erst auf Basis dieser Informationen ist er in der Lage, eingehende Telefonate oder Nachrichten zuzustellen.
</p>
</details>

---

### (6) Diskussion und Zusammenführung
**Ziele:**
* Die Lernenden erklären, dass verschiedene (Meta-)Daten ein Gesamtbild ergeben
* Die Lernenden bewerten, dass Daten bzw. Metadaten in positiven wie negativen Kontexten angewendet werden können (staatlich wie privatwirtschaftlich)

**Einleitung:**
Wir haben viele Daten gesammelt, was für ein Bild ergibt sich daraus?

**Medium:**
Zitat: *"To every man is given the key to the gates of heaven; the same key opens the gates of hell."* - Richard P. Feynman

**Fragen & Antworten:**
* Wer mag das Zitat übersetzen?
    * "Jeder Mensch hat den Schlüssel zum Himmel erhalten; der gleiche Schlüssel öffnet aber auch das Tor zur Hölle."
* Wer kann sich erklären, warum es hier am Ende dieses Workshops steht?
    * Daten, die wir jeden Tag preisgeben, ermöglichen uns praktische Dienste wie beispielsweise soziale Netzwerke oder digitale Assistenten und sind eine Bereicherung für unser Leben. Auf der anderen Seite hinterlassen wir aber auch viele Daten, die später gegen uns verwendet werden können und über die wir die Kontrolle verloren haben.

**Weiterführende didaktische Ideen:**
* Mit den Lernenden kann diskutiert werden, inwiefern der Inhalt des Zitates auch auf die Atomtechnologie zutraf, auf die sich das Zitat von Feynman ursprünglich bezog.
* Von Atomstrom kann man sich fernhalten (erneuerbare Energien), ohne auf Strom verzichten zu müssen. Welche Möglichkeiten hat man im digitalen Raum, die positiven Aspekte zu nutzen und die Risiken zu minimieren? (bewusst mit den eigenen Daten umgehen, alternative Dienste nutzen/fördern, politische Regulierung erkämpfen etc.)

**Hintergrundinformationen:**
* Quelle des Zitats: https://archive.nytimes.com/www.nytimes.com/books/first/f/feynman-meaning.html

---

### Lizenz
*Inhalt:* [Chaos macht Schule](https://ccc.de/schule)
*Konzept:* [die Community des edulabsBE](https://edulabs.de/projects/edulabsBE/)
*Redaktionelle Unterstützung:* [Maximilian Voigt](https://twitter.com/ma__vo)
*Medien:* 
* [Video zum Thema "Netzspionage" - Metversa e.V.](http://metaversa.de/web/projekte/mobiles-lernen/netzspionage-leicht-gemacht/)
* Facebook Friends-Graph - Yorn / Chaos macht Schule

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br/><p>Die Inhalte dieser Seite sind, sofern nicht anders angegeben, nach <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons 4.0</a> Attribution lizenziert.</p>

---

<center>
Unterstützt durch:<br>
<a href="https://edulabs.de/"><img src="https://github.com/okfde/edulabs/blob/master/assets/img/static/edulabs-logo.png?raw=true" alt="edulabs Logo" style="width:125px;"></a></center>