# Workflow zur OER-Erstellung nach Axel Dürkop et al. (2019)

„Freie Bildung für alle&quot; ist das Motto von OER, den Open Educational Resources. Offiziell werden diese wie folgt definiert:

„Open Educational Resources (OER) sind jegliche Arten von Lehr-Lern-Materialien, die gemeinfrei oder mit einer freien Lizenz bereitgestellt werden. Das Wesen dieser offenen Materialien liegt darin, dass jedermann sie legal und kostenfrei vervielfältigen, verwenden, verändern und verbreiten kann. OER umfassen Lehrbücher, Lehrpläne, Lehrveranstaltungskonzepte, Skripte, Aufgaben, Tests, Projekte, Audio-, Video- und Animationsformate. (VON JÖRAN MUUSS-MERHOLZ FÜR [OERINFO – TRANSFERSTELLE FÜR OER](http://open-educational-resources.de/) UNTER [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de). DER ENGLISCHSPRACHIGE ORIGINALTEXT DER UNESCO IST UNTER [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/deed.de) LIZENZIERT)_

Die TIB Niedersachsen prüft momentan, wie diese Bedingungen technisch bewerkstelligt werden können und erstellt ein OER-Portal für Niedersachsen. Bis dieses Portal erstellt ist, gibt es jedoch eine Übergangslösung für die OER-Erstellung, die im Folgenden erläutert wird.


Bei der Erstellung von OER-Materialien handelt es sich dabei weniger um einen lineares als ein zirkuläres System. Denn die Materialien stehen nach Vollendung zur Nachnutzung zur Verfügung und können das gleiche System mehrmals durchlaufen. Das System besteht aus drei Phasen: Erstellungsphase, Formatierungsphase und Verwertungsphase. Diese wurden einem Modell über Modernes Publizieren von Alex Dürkop und Florian Hagen nachempfunden.

### Erstellungsphase

Die _Erstellungsphase_ beschreibt den Schreibprozess von einer oder mehreren Personen, die allein oder gemeinsam, synchron oder asynchron zu einem **ersten Entwurf ihres OER-Materials** kommen. Plattformen, um kollaborativ zu arbeiten, sind z.B. Hackmd.io und Google Docs. Wichtig hierbei ist, dass der Schreibende sich in seiner Schreibumgebung wohlfühlt. Denn der Schreibprozess sollte nicht unter der Schreibumgebung bzw. dem zum Schreiben genutzten Werkzeug leiden.

![WorkflowBild](Inhalt/Bilder/workflow.jpg)

### Formatierungsphase

Die _Formatierungsphase_ in der Mitte des Workflows ist eine Phase des Verarbeitungsprozesses. Der OER-Beitrag wird vor der Einreichung technisch aufbereitet und kann erste Zyklen der Qualitätssicherung durchlaufen.

Für die Formatierungsphase gibt es eine neue Rolle für die Formatierung und zwar **die Rolle des Moderators**. Er oder Sie ist in der Abbildung grün dargestellt. Damit alle den OER-Beitrag nutzen können, bedarf es offener Formate, die verändert werden können. Die OER-Erstellenden werden von dem Moderator bei dem **Verarbeitungs- und Veröffentlichungsprozess** unterstützt. Die Rolle des Moderators kümmert sich z.B. um die Übersetzung des OER-Beitrages in das offene Format Markdown und die Veröffentlichung auf der Plattform Gitlab. Auf Gitlab können die OER-Beiträge in diverse Formate konvertiert werden: z.B. HTML, PDF, EPUB und DOC oder DOCX.  Eine Version der Datei sollte dann in einem Repositorium hochgeladen werden. In unserem Schaubild wird als Beispiel für ein solches das &quot;Göttingen Research Online&quot;-Repositorium verwendet. Den Nutzen dieses Schrittes wird in der Verwertungsphase deutlich.

Zum jetzigen Zeitpunkt des Projekts ist es so, dass die Rolle des Moderators die Erstellung der OER-Beiträge begleitet. Zu einem späteren Zeitpunkt des Projektes können die OER-Ersteller diesen Schritt vielleicht auch selbst durchführen. Mithilfe eines Converters können sie einfach ihre erarbeiteten Dateien konvertieren und auf Gitlab hochladen. Es ist davon auszugehen, dass sich diese Phase mit der Eröffnung des Portals der TIB verselbstständigt. Die OER-Beiträge werden dann nicht mehr in Gitlab und einem Repositorium, sondern direkt auf dem Portal der TIB veröffentlicht.

### Verwertungsphase

Die _Verwertungsphase_ rechts auf dem Schaubild zeigt die Nutzung der Dateien durch das Repositorium. So stehen die Beiträge bestmöglich zur **Nutzung und Nachnutzung** zur Verfügung. Die Beiträge können vervielfältigt, verwendet, verarbeitet, vermischt und verbreitet werden. Entsteht ein neues Produkt, durchläuft es das gleiche System erneut. Das bestehende Material kann außerdem direkt auf Gitlab verändert und angepasst werden, was den gesamten Workflow stark vereinfacht. Im Repositorium können dann die neuen Versionen der OER-Beiträge hochgeladen werden. Die Links zum Beitrag auf dem Repositorium zeigen dann sowohl die alten als auch die neuen Versionen an. Daher können Links nicht mehr veralten und müssen auch nicht mehr ausgetauscht werden.

#### Quellen

Dürkop, Alex / Hagen, Florian: Ein sozio-technisches System zum kollaborativen Schreiben und Publizieren. In: Modernes Publizieren, 09.06.2019. URL: [https://oa-pub.hos.tuhh.de/de/2019/06/09/ein-sozio-technisches-system-zum-kollaborativen-schreiben-und-publizieren/#pre-submission-stage-die-phase-vor-der-einreichung](https://oa-pub.hos.tuhh.de/de/2019/06/09/ein-sozio-technisches-system-zum-kollaborativen-schreiben-und-publizieren/#pre-submission-stage-die-phase-vor-der-einreichung).