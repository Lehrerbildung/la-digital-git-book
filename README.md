# Gitbook für das niedersächsische Verbundprojekt "Basiskompetenzen Digitalisierung"

Das Entwicklungsprojekt [„Basiskompetenzen Digitalisierung“](http://www.lehrerbildungsverbund-niedersachsen.de/index.php?s=ProjektBasiskompetenzenDigitalisierung)
ist auf Initiative des *Niedersächsischen Verbunds zur Lehrerbildung* entstanden und wird durch das *Niedersächsische Ministerium für Wissenschaft und Kultur* (MWK) finanziert.

Ziel des Verbundprojekts ist es, Materialien und Instrumente zur Förderung von Basiskompetenzen zur 
Digitalisierung im Dialog mit allen niedersächsischen lehrerbildenden Hochschulen zu entwickeln, um diese
langfristig – standortspezifisch entweder additiv oder integrativ – in das Regelstudium aller
niedersächsischen Lehramtsstudiengänge zu integrieren.

Die OER-Materialien, die im Projekt kuratiert und entwickelt werden, sollen nach und nach über diese Plattform veröffentlich werden.
Perspektivisch sollen alle Materialien auch über das Niedersächsische OER-Portal, das aktuell erst entwickelt wird, frei zugänglich gemacht werden.



------------------------------------------------------------------------


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br>

Dieses Projekt ist eine Fork des [OERientation](https://oerientation.rz.tuhh.de/)-Projektes des
[Instituts für Technische Bildung und Hochschuldidaktik (ITBH)](http://itbh-hh.de/de/) an der
[TU Hamburg-Harburg](https://www.tuhh.de/) und wurde dort von Sabrina Maaß entwickelt. 
Das Projekt ist lizenziert unter einer [Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz](https://creativecommons.org/licenses/by-sa/4.0/legalcode.de).

